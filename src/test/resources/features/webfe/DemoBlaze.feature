@DemoBlaze @WebTesting
Feature: Online Shop
  As a common user
  I want to search, add, remove products and purchase
  So that validate the Demo Online Shop behaviour

  @OnlineShop
  Scenario Outline: Searching, adding, removing products and purchase
    Given User goes to Demo Online Shop
    When he goes to "Phones" category
    When he goes to "Laptops" category
    When he goes to "Monitors" category
    When he adds a "Laptops" - <product 1> costing <price 1>
    When he adds a "Laptops" - <product 2> costing <price 2>
    When he deletes <product 2> from Cart
    When he places order and validate amount is "<price 1> USD"
      | Name        | <Name>     |
      | Country     | <Country>  |
      | City        | <City>     |
      | Credit card | <CC>       |
      | Month       | <CC Month> |
      | Year        | <CC Year>  |

    Examples:
      | product 1      | price 1 | product 2     | price 2 |  Name           |  Country |  City    |  CC                |  CC Month |  CC Year |
      | "Sony vaio i5" | 790     | "Dell i7 8gb" | 700     | "Jose Castillo" | "Spain"  | "Madrid" | "1234123412341234" | "10"      | "22"     |