@PetStore @API_Testing
Feature: Pet Store
  As a common user
  I want to get available pets and complete the creation, updating and deleting of a new pet
  So that validate Pet Store rest api responses

  @PetStoreMaintainer
  Scenario: Get available pets, create new pet, update added pet and remove modified pet
    Given User defines base url: "https://petstore.swagger.io/v2"
    #Get available pets
    When he defines "Get" request type
    When he defines "/pet/findByStatus" request URI path
    When he defines request query parameters
      | status | available |
    When he sends the request
    Then he validates response status code is: 200
    Then he validates that all response nodes matching "findAll{pet -> 1==1}.status" jsonpath equal: "available"
    #New available pet to the store
    When he defines "Post" request type
    When he defines "/pet" request URI path
    When he uses "NewPet" file json for request body, replacing jsonpaths matches by values
      | $.status      | available |
      | $.name        | Perlita   |
      | $.tags.[0].id | 9         |
    When he defines request headers
      | Content-Type | application/json |
    When he sends the request
    Then he validates response status code is: 200
    When he stores "id" from response body in "Added pet ID"
    When he defines "Get" request type
    When he defines "/pet/{petId}" request URI path
    When he adds stored "Added pet ID" as "petId" request path parameter
    When he sends the request
    Then he validates response status code is: 200
    When he stores the response body as "Added pet Json"
    #Update this pet to sold
    When he defines "Put" request type
    When he defines "/pet" request URI path
    When he uses "Added pet Json" stored json for request body, replacing jsonpaths matches by values
      | $.status | sold |
    When he defines request headers
      | Content-Type | application/json |
    When he sends the request
    Then he validates response status code is: 200
    When he defines "Get" request type
    When he defines "/pet/{petId}" request URI path
    When he adds stored "Added pet ID" as "petId" request path parameter
    When he sends the request
    Then he validates response status code is: 200
    Then he validates that response node matching "status" jsonpath equals: "sold"
    #Delete this pet
    When he defines "Delete" request type
    When he defines "/pet/{petId}" request URI path
    When he adds stored "Added pet ID" as "petId" request path parameter
    When he sends the request
    Then he validates response status code is: 200
    When he defines "Get" request type
    When he defines "/pet/{petId}" request URI path
    When he adds stored "Added pet ID" as "petId" request path parameter
    When he sends the request
    Then he validates response status code is: 404
    Then he validates that response node matching "message" jsonpath equals: "Pet not found"