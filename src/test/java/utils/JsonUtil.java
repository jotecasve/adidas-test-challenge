package utils;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.json.JSONObject;

public class JsonUtil {
    private static final String jsonRequestsLocation = "src/test/java/utils/restdata/";

    public static JSONObject getRequestJsonObject(String jsonName) {
        return getJsonObjectGivenLocationAndJsonName(jsonRequestsLocation, jsonName);
    }

    public static JSONObject getJsonObjectGivenLocationAndJsonName(String jsonLocationInsideProject, String jsonName) {
        String jsonString = FileUtil.getFileStringGivenPath(jsonLocationInsideProject + jsonName + ".json");
        return new JSONObject(jsonString);
    }

    public static JSONObject replaceValueInJsonGivenJsonPath(JSONObject jsonObject, String jsonPath, Object newValue) {
        DocumentContext doc = JsonPath.parse(jsonObject.toString()).set(jsonPath, newValue);
        return new JSONObject(doc.jsonString());
    }
}
