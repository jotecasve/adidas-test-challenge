package pages.DemoBlaze.PopUps;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import services.webfe.WebActions;

public class OrderPopup extends PageObject {

    static String popupXpath = "//div[@id='orderModal']//div[@class='modal-content'][.//h5[text()='Place order']]";
    static Target popup = Target.the("Place Order Popup")
            .locatedBy(popupXpath);

    public static void validateLoadedCorrectly() {
        WebActions.waitsUntilVisible(popup);
    }

    public static void fillField(String fieldName, String inputText){
        WebActions.attemptsToFillTextBox(fieldName+" text box",popupXpath+"//div[@class='form-group'][./label[text()='"+fieldName+":']]/input", inputText, "fills '"+fieldName+"' webform field with: "+inputText);
    }

    public static void clickButton(String buttonName){
        WebActions.attemptsToClick(buttonName+" button",popupXpath+"//button[text()='"+buttonName+"']", "clicks on '"+buttonName+"' button");
    }
}