package pages.DemoBlaze.PopUps;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import services.webfe.WebActions;

public class PurchasePopup extends PageObject {

    static String popupXpath = "//div[contains(@class,'sweet-alert ')][.//h2[text()='Thank you for your purchase!']]";
    static Target popup = Target.the("Successful Purchase Popup")
            .locatedBy(popupXpath);
    static Target purchaseInfo = Target.the("Purchase Information")
            .locatedBy(popupXpath + "//p[@class='lead text-muted ']");
    static Target okButton = Target.the("OK Button")
            .locatedBy(popupXpath + "//button[text()='OK']");


    public static void validateLoadedCorrectly() {
        WebActions.waitsUntilVisible(popup);
    }

    public static void validateClosedCorrectly() {
        WebActions.waitsUntilNotPresent(popup);
    }

    public static void clickOKButton(){
        WebActions.clickAction(okButton,"clicks on OK button");
    }

    public static void validateFieldEquals(String fieldName, String fieldValue) {
        WebActions.assertsFieldTextMatches(purchaseInfo, "^(.*[\\n])*" + fieldName + ": " + fieldValue + "([\\n].*)*$");
    }

    public static void storeFieldValue(String fieldName, String storeKey) {
        String purchaseInfoRows = WebActions.getFieldText(purchaseInfo);
        String fieldValue = "";
        for (String purchaseInfoRow : purchaseInfoRows.split("\n")) {
            if (purchaseInfoRow.startsWith(fieldName + ": ")) {
                fieldValue = purchaseInfoRow.substring(fieldName.length() + 2);
                break;
            }
        }
        WebActions.saveValue(storeKey, fieldValue);
    }
}