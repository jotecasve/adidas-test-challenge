package pages.DemoBlaze;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import services.webfe.WebActions;

public class ProductPage extends PageObject {

    static Target ADD_TO_CART_BUTTON = Target.the("Add to Cart button")
            .locatedBy("//a[text()='Add to cart']");

    private static Target getTitle(String product) {
        return Target.the("Product Page Title: " + product)
                .locatedBy("//h2[text()='" + product + "']");
    }

    public static void validateLoadedCorrectly(String product) {
        WebActions.waitsUntilVisible(getTitle(product));
    }

    public static void addProductToCart() {
        WebActions.clickAction(ADD_TO_CART_BUTTON, "adds product to cart");
    }

    public static void acceptProductAddedAlert() {
        WebActions.acceptsAlertWithMessage("Product added");
    }

}
