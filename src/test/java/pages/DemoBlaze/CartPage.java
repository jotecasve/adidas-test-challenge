package pages.DemoBlaze;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import services.webfe.WebActions;

public class CartPage extends PageObject {

    static String TABLE_HEADER_PRICE = "Price";
    static String TABLE_HEADER_TITLE = "Title";
    static Target PRODUCTS_TITLE_LABEL = Target.the("Products Title")
            .locatedBy("//h2[text()='Products']");
    static Target PLACE_ORDER_BUTTON = Target.the("Place Order")
            .locatedBy("//button[text()='Place Order']");

    private static Target productRow(String title) {
        return Target.the("Product row")
                .locatedBy(getXpathForRowTitle(title));
    }

    private static Target deleteProduct(String title) {
        return Target.the("Delete Product")
                .locatedBy(getXpathForRowTitle(title) + "//td/a[.//text()[.='Delete']]");
    }

    public static void validateCartPageLoadedCorrectly() {
        WebActions.waitsUntilVisible(PRODUCTS_TITLE_LABEL);
    }

    public static void placeOrder() {
        WebActions.clickAction(PLACE_ORDER_BUTTON,"clicks on Place Order button");
    }

    public static void validateProductWasAdded(String title, int price) {
        WebActions.waitsUntilVisible(productRow(title));
        WebActions.waitsUntilVisible("Product row with expected price", getXpathForRowTitle(title) + getXpathForRowColumnAndValue(TABLE_HEADER_PRICE, "" + price));
    }

    public static void validateProductWasRemoved(String title) {
        WebActions.waitsUntilNotPresent(productRow(title));
    }

    public static void removeProductFromCart(String title) {
        WebActions.clickAction(deleteProduct(title), "removes "+title+" Product");
    }

    private static String getXpathForRowTitle(String title) {
        return "//table/tbody/tr" + getXpathForRowColumnAndValue(TABLE_HEADER_TITLE, title);
    }

    private static String getXpathForRowColumnAndValue(String columnName, String cellValue) {
        return "[./td[" + getXpathColumnNumber(columnName) + "][text()='" + cellValue + "']]";
    }

    private static String getXpathColumnNumber(String columnName) {
        return "count(//table/thead/tr/th[.//text()[.='" + columnName + "']]/preceding-sibling::*)+1";
    }
}