package pages;

import net.serenitybdd.screenplay.Actor;
import services.webfe.WebActions;

public class NavigateTo {
    public static void setPage(Actor actor, String pageName, Class pageClass) {
        WebActions.navigatesToPage(actor, pageName, pageClass);
    }
}
