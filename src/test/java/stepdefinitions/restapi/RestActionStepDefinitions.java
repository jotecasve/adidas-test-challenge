package stepdefinitions.restapi;

import io.cucumber.java.en.Then;
import steps.restapi.RestActionSteps;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Steps;

public class RestActionStepDefinitions {

    @Steps
    private RestActionSteps restActionSteps;

    @Given("{actor} defines base url: {string}")
    public void customerDefinesBaseUrlForRequests(Actor actor, String baseUrl) {
        restActionSteps.setRequestBaseUrl(actor, baseUrl);
    }

    @When("he defines {string} request type")
    public void heDefinesRequestType(String requestType) {
        restActionSteps.setRequestType(requestType);
    }

    @When("he defines {string} request URI path")
    public void heDefinesRequestURIPath(String requestURIPath) {
        restActionSteps.setRequestURIPath(requestURIPath);
    }

    @When("he defines request headers")
    public void heDefinesRequestHeaders(DataTable dataTable) {
        restActionSteps.addHeaders(dataTable);
    }

    @When("he defines request query parameters")
    public void heDefinesRequestQueryParameters(DataTable dataTable) {
        restActionSteps.addQueryParameters(dataTable);
    }

    @When("he defines request path parameters")
    public void heDefinesRequestPathParameters(DataTable dataTable) {
        restActionSteps.addPathParameters(dataTable);
    }

    @When("he adds stored {string} as {string} request path parameter")
    public void heAddsStoredAsRequestPathParameter(String storeKey, String pathParameter) {
        restActionSteps.addStoredValueAsPathParameter(storeKey, pathParameter);
    }

    @When("he uses {string} file json for request body, replacing jsonpaths matches by values")
    public void heUsesFileJsonForRequestBodyReplacingValues(String jsonName, DataTable dataTable) {
        restActionSteps.attachFileJsonToBody(jsonName, dataTable);
    }

    @When("he uses {string} stored json for request body, replacing jsonpaths matches by values")
    public void heUsesStoredJsonForRequestBodyReplacingValues(String storeKey, DataTable dataTable) {
        restActionSteps.attachStoredJsonToBody(storeKey, dataTable);
    }

    @When("he sends the request")
    public void heSendsTheRequest() {
        restActionSteps.sendRequest();
    }

    @Then("he validates response status code is: {int}")
    public void heValidatesResponseStatusCodeIs(int statusCode) {
        restActionSteps.validateStatusCode(statusCode);
    }

    @Then("he validates that all response nodes matching {string} jsonpath equal: {string}")
    public void heValidatesThatAllResponseNodesMatchingJsonpathEquals(String jsonPath, Object expectedEqualValue) {
        restActionSteps.validateAllResponseNodesEqual(jsonPath, expectedEqualValue);
    }

    @Then("he validates that response node matching {string} jsonpath equals: {string}")
    public void heValidatesThatResponseNodeMatchingJsonpathEquals(String jsonPath, Object expectedEqualValue) {
        restActionSteps.validateResponseNodeEquals(jsonPath, expectedEqualValue);
    }

    @Then("he stores {string} from response body in {string}")
    public void heStoresFromResponseBodyIn(String jsonPath, String storeKey) {
        restActionSteps.storeNodeValueFromResponseBody(jsonPath, storeKey);
    }

    @When("he stores the response body as {string}")
    public void heStoresTheResponseBody(String storeKey) {
        restActionSteps.storeResponseBody(storeKey);
    }
}
