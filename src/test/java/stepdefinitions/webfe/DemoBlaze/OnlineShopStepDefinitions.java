package stepdefinitions.webfe.DemoBlaze;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Given;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Steps;
import steps.webfe.DemoBlaze.OnlineShopSteps;

public class OnlineShopStepDefinitions {

    @Steps
    private OnlineShopSteps onlineShopSteps;

    @Given("{actor} goes to Demo Online Shop")
    public void goesToDemoOnlineShop(Actor actor) {
        onlineShopSteps.navigateToDemoOnlineShop(actor);
    }

    @When("he goes to {string} category")
    public void goesToCategory(String category) {
        onlineShopSteps.goesToCategory(category);
    }

    @When("he adds a {string} - {string} costing {int}")
    public void addAProductCostingPrice(String category, String product, int price) {
        onlineShopSteps.addProduct(category, product, price);
    }

    @When("he deletes {string} from Cart")
    public void removeProductFromCart(String product) {
        onlineShopSteps.removeProduct(product);
    }

    @When("he places order and validate amount is {string}")
    public void placeOrderAndValidateAmountIs(String amount, DataTable dataTable) {
        onlineShopSteps.placeOrderAndValidateAmount(amount, dataTable);
    }
}
