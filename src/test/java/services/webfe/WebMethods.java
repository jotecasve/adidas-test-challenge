package services.webfe;

import io.cucumber.datatable.DataTable;

import java.util.HashMap;

public class WebMethods {
    public static HashMap<Object, Object> convertDataTableToMap(DataTable dataTable) {
        return new HashMap<Object, Object>(dataTable.asMap(Object.class, Object.class));
    }

    public static String removeSurroundingDoubleQuotesFromString(String string) {
        return string.replaceAll("^\"|\"$", "");
    }
}
