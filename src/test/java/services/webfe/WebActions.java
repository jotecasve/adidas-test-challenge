package services.webfe;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.FileUtil;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*;
import static net.serenitybdd.screenplay.targets.Target.the;

public class WebActions {

    public static void clickAction(Target target, String actionDescription) {
        theActorInTheSpotlight().attemptsTo(Task.where("{0} " + actionDescription, Click.on(target)));
    }

    public static void waitsUntilVisible(String elementName, String elementLocator) {
        waitsUntilVisible(getTargetElementNameAndLocator(elementName, elementLocator));
    }

    public static void waitsUntilVisible(Target target) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(target, isVisible()));
    }

    public static void waitsUntilNotPresent(Target target) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(target, isNotPresent()));
    }

    private static Target getTargetElementNameAndLocator(String elementName, String elementLocator) {
        return the(elementName).locatedBy(elementLocator);
    }

    public static void navigatesToPage(Actor actor, String pageName, Class pageClass) {
        actor.wasAbleTo(Task.where("{0} opens the " + pageName + " page",
                Open.browserOn().the(pageClass)));
    }

    public static void attemptsToClick(String elementName, String elementLocator, String actionDescription) {
        clickAction(getTargetElementNameAndLocator(elementName, elementLocator), actionDescription);
    }

    public static void assertsFieldTextMatches(Target target, String matchingRegEx) {
        theActorInTheSpotlight().attemptsTo(Ensure.that(target).text().matches(matchingRegEx));
    }

    public static void attemptsToFillTextBox(String elementName, String elementLocator, String text, String actionDescription) {
        attemptsToFillTextBox(getTargetElementNameAndLocator(elementName, elementLocator), text, actionDescription);
    }

    public static void attemptsToFillTextBox(Target target, String text, String actionDescription) {
        theActorInTheSpotlight().attemptsTo(Task.where("{0} " + actionDescription, Enter.theValue(text).into(target)));
    }

    public static String getFieldText(Target target) {
        return Text.of(target).viewedBy(theActorInTheSpotlight()).asString();
    }

    static String STORED_VALUES = "target/output_data.txt";

    public static void saveValue(String storeKey, String storeValue) {
        theActorInTheSpotlight().remember(storeKey, storeValue);
        FileUtil.appendText(STORED_VALUES, storeKey + ":\n\t" + storeValue.replace("\n", "\n\t")+"\n");
    }

    public static String recallsValue(String storeKey) {
        return theActorInTheSpotlight().recall(storeKey);
    }

    public static void acceptsAlertWithMessage(String message) {
        BrowseTheWeb.as(theActorInTheSpotlight()).waitFor(ExpectedConditions.alertIsPresent());
        Alert alert = BrowseTheWeb.as(theActorInTheSpotlight()).getAlert();
        String alertText = alert.getText();
        alert.accept();
        theActorInTheSpotlight().attemptsTo(Ensure.that(alertText).contains(message));
    }
}
