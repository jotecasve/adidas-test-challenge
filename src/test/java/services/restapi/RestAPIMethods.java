package services.restapi;

import net.serenitybdd.rest.SerenityRest;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.*;

public class RestAPIMethods {

    public static String getNodeStringFromJsonResponseBody(String jsonPath) {
        return SerenityRest.lastResponse().path(jsonPath).toString();
    }

    public static String getJsonResponseBody() {
        return SerenityRest.lastResponse().getBody().asPrettyString();
    }

    public static void validateResponseStatusCode(int statusCode) {
        theActorInTheSpotlight().should(
                seeThatResponse("The response status code is the expected one '" + statusCode + "'",
                        response -> response.statusCode(statusCode))
        );
    }

    public static void validateAllValuesOfResponseNodeEquals(String nodeJsonPath, Object nodeExpectedValue) {
        theActorInTheSpotlight().should(
                seeThatResponse("all the values of response body for '" + nodeJsonPath + "' jsonpath equal: " + nodeExpectedValue,
                        response -> response.body(nodeJsonPath, hasItems(nodeExpectedValue)))
        );
    }

    public static void validateResponseNodeValueEquals(String nodeJsonPath, Object nodeExpectedValue) {
        theActorInTheSpotlight().should(
                seeThatResponse("value of response body for '" + nodeJsonPath + "' jsonpath equal: " + nodeExpectedValue,
                        response -> response.body(nodeJsonPath, equalTo(nodeExpectedValue)))
        );
    }
}
