package steps.webfe.DemoBlaze;

import net.serenitybdd.screenplay.Actor;
import pages.DemoBlaze.PopUps.OrderPopup;
import pages.DemoBlaze.PopUps.PurchasePopup;
import pages.DemoBlaze.CartPage;
import pages.DemoBlaze.HomePage;
import pages.DemoBlaze.ProductPage;
import pages.NavigateTo;
import services.webfe.WebMethods;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Step;

import java.util.Map;

public class OnlineShopSteps {

    @Step
    public void navigateToDemoOnlineShop(Actor actor) {
        NavigateTo.setPage(actor, "Demo Blaze", HomePage.class);
    }

    @Step
    public void goesToCategory(String category) {
        HomePage.selectCategory(category);
        HomePage.validateCategoryVisible(category);
    }

    @Step
    public void addProduct(String category, String product, int price) {
        HomePage.goToNavbarOption("Home");
        HomePage.selectCategory(category);
        HomePage.navigateToProduct(product);
        ProductPage.validateLoadedCorrectly(product);
        ProductPage.addProductToCart();
        ProductPage.acceptProductAddedAlert();
        HomePage.goToNavbarOption("Cart");
        CartPage.validateCartPageLoadedCorrectly();
        CartPage.validateProductWasAdded(product, price);
    }

    @Step
    public void removeProduct(String product) {
        HomePage.goToNavbarOption("Cart");
        CartPage.validateCartPageLoadedCorrectly();
        CartPage.removeProductFromCart(product);
        CartPage.validateProductWasRemoved(product);
    }

    @Step
    public void placeOrderAndValidateAmount(String amount, DataTable dataTable) {
        HomePage.goToNavbarOption("Cart");
        CartPage.validateCartPageLoadedCorrectly();
        CartPage.placeOrder();
        OrderPopup.validateLoadedCorrectly();
        Map<Object, Object> customerDataSet = WebMethods.convertDataTableToMap(dataTable);
        for (Object customerDataKey : customerDataSet.keySet()) {
             OrderPopup.fillField(customerDataKey.toString(), WebMethods.removeSurroundingDoubleQuotesFromString(customerDataSet.get(customerDataKey).toString()));
        }
        OrderPopup.clickButton("Purchase");
        PurchasePopup.validateLoadedCorrectly();
        PurchasePopup.storeFieldValue("Id", "Purchase ID");
        PurchasePopup.storeFieldValue("Amount", "Purchase Amount");
        PurchasePopup.validateFieldEquals("Amount", amount);
        PurchasePopup.clickOKButton();
        PurchasePopup.validateClosedCorrectly();
    }
}
