# Adidas Test Automation Challenge
---
## API REST and Web Front End Testing - Exercise solution

---
## Project structure

- adidas-test-automation
  - src/test/java
    - pages ->  contains the POM Pattern with the **web elements (Target)** and **methods specific** for **each page**
    - services -> cross or generic methods and actions.
    - stepdefinitions -> **a method** for **each step in Cucumber files**
    - steps -> have the **implemented methods** for **each Step Definition method**
    - utils -> Reusable functions for **Files and Jsons** management.
  - src/test/resources -> feature files with gherkin syntax, used as documentation of tests. Sentences in human language with **Given** [Pre-condition], **When** [Action] and **Then** [Validation] prefixes
    - features/restapi
    - features/webfe
  - serenity.properties -> global serenity project configurations

## Run Instructions

First, execute in the terminal the following command:

```sh
mvn clean install -DskipTests
```

Then, execute the tests:

```sh
mvn verify
```

The **previous command** will execute all the tests inside this path **"/src/test/resources/features/"**, it involves rest api and web front end tests.

Also, there is an option to filter tests by using tags, for example if you want to execute the test with a tag=WebTesting, you can execute the following command:

```sh
mvn verify -Dtags="@WebTesting"
```

By **default** the Web Front End Tests will run in **Chrome** browser, to **run with another** one, add **"-Dwebdriver.driver"** to the command with values like **"edge"** or **"firefox"**.

```sh
mvn verify -Dwebdriver.driver=edge
```
### Reports

**Serenity Result Report** will be generated in **"/target/site/serenity/index.html"**.
  - Summary tab: Global test suite execution results (Scenario Results including rows of test data) and the Functional Coverage Overview.
  - Test Results tab: Automated Tests and Manual Tests details for the test suite execution (steps, start time, duration, results)

---
## Next actions for this project

The suggested next action items in order to improve this project are:

- Use some documentation apps like javadocs in order to have a **better documentation** of used methods and classes.
- Complete scripts using **integrated flows** with api and front end tests in the same scenario.
- **Test data driven** for different environments.
- Testing in **multiple browsers** (Chrome, Firefox, Safari)
- **Parallel test executions** in containers.
- Cloud executions in order to ensure the **CI/CD with continuous testing**. Tools like: Jenkins, Docker and BrowserStack.